import sqlite3

class Sistema_Gestion_Academica:

    def __init__(self):
        self.conexion = sqlite3.connect('academia.sqlite')
        self.cursor = None
        self.datos_estudiante = list()
        print("Conexion establecida !!!")


    def registrar_estudiante(self):
        print("****************************************************")
        print(" registrando ")
        num_est = int(input(" Ingrese numero de estudiante a registar: "))
        for i in range(num_est):
            name=input(" ingrese nombre: ")
            email = input(" ingrese email: ")
            self.datos_estudiante.append((name,email))
        print(self.datos_estudiante)
        print(" registados ")



    def crear_base_datos(self):
        sql_estudiante = """CREATE TABLE Estudiante (
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, 
        name   TEXT UNIQUE, 
        email  TEXT)"""

        sql_course = """ CREATE TABLE Course (
        id      INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
        title   TEXT UNIQUE
        )"""
        sql_member = """ CREATE TABLE Member (
        user_id     INTEGER,
        course_id   INTEGER,
        role        INTEGER,
        PRIMARY KEY (user_id, course_id)
        ) """

        self.cursor = self.conexion.cursor()

        #self.cursor.execute(sql_estudiante)
        #self.cursor.execute(sql_course)
        #self.cursor.execute(sql_member)

        print("Creadas las tablas !!!")
        self.cursor.close()


    def insertar_datos_estudiante(self):
        self.cursor = self.conexion.cursor()
        self.cursor.executemany("INSERT INTO Estudiante (name, email) VALUES(?, ?)",
                                self.datos_estudiante)
        self.conexion.commit()
        self.cursor.close()
        print("datos insertados")


    def insertar_courses(self):
        self.cursor = self.conexion.cursor()
        self.cursor.executemany("INSERT INTO Course(title) VALUES(?)",
                                [('Programación',),
                                 ('Evaluación de los Aprendizajes',),
                                 ('Educación No Formal',)])
        self.conexion.commit()
        self.cursor.close()
        print ("Cursos insertados")

    def insertar_members(self):
        self.cursor = self.conexion.cursor()
        try:
            self.cursor.executemany("INSERT INTO Member(user_id, course_id, role) VALUES(?, ?, ?)",
                                [(1, 1, 1),
                                 (2, 1, 0),
                                 (3, 1, 0),
                                 (1, 2, 0),
                                 (2, 2, 1),
                                 (2, 3, 1),
                                 (3, 3, 0)
                                ])
            self.conexion.commit()
            self.cursor.close()
            print("Miembros insertados")

        except sqlite3.Error as ex:
            print ("Error: ", ex)



    def visualizar_datos_estudiante(self):
        print("---------------------------------------------")
        visualizar="""SELECT * FROM Estudiante"""
        self.cursor = self.conexion.cursor()
        self.cursor.execute(visualizar)
        filas = self.cursor.fetchall()
        print(" Mostrando datos estudiantes ingresados ")
        for f in filas:
            print (f)
        self.cursor.close()

    def matricular(self):
        print("-------------------------------------------")
        matricula = """SELECT Estudiante.name, Course.title
                    FROM Estudiante JOIN Course
                    ON Estudiante.id = Course.id
                 """
        self.cursor = self.conexion.cursor()
        self.cursor.execute(matricula)
        filas = self.cursor.fetchall()
        print(" matriculados: ")
        for f in filas:
            print(f)
        self.cursor.close()



    def buscar_estudiante(self):
        print("----------------------------------------------")
        print(" Buscar por id ")
        buscar="""SELECT * FROM Estudiante WHERE id='6'
                """
        self.cursor = self.conexion.cursor()
        self.cursor.execute(buscar)
        filas = self.cursor.fetchall()
        for f in filas:
            print(f)
        self.cursor.close()
        print(" Encontrado ")



    def eliminar_estudiante(self):
        print("---------------------------------------")
        print(" Eliminar por id: ")
        self.cursor = self.conexion.cursor()
        #eliminar="""DELETE FROM Estudiante WHERE name='freddy'"""
        self.cursor.execute("""DELETE FROM Estudiante WHERE id='5'""")
        self.conexion.commit()
        self.cursor.close()
        print(" Eliminado ")


    def actualizar(self):
        print("--------------------------------------------")
        print(" Actualizando ")
        actualizar = """SELECT * FROM Estudiante
                     """
        self.cursor = self.conexion.cursor()
        self.cursor.execute(actualizar)
        filas = self.cursor.fetchall()
        for f in filas:
            print(f)
        self.cursor.close()


if __name__ == "__main__":

    base = Sistema_Gestion_Academica()
    #base.crear_base_datos()
    #base.insertar_members()
    #base.registrar_estudiante()
    #base.insertar_datos_estudiante()
    base.visualizar_datos_estudiante()
    base.matricular()
    base.buscar_estudiante()
    base.eliminar_estudiante()
    base.actualizar()
